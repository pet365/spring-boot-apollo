package com.tianju.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
//@ConfigurationProperties(prefix = "mysql")
@Data
public class MyConfig {
    private String username;
    private String password;
}
