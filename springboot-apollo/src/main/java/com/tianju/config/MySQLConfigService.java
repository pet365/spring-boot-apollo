package com.tianju.config;

import com.ctrip.framework.apollo.Config;
import com.ctrip.framework.apollo.model.ConfigChangeEvent;
import com.ctrip.framework.apollo.spring.annotation.ApolloConfig;
import com.ctrip.framework.apollo.spring.annotation.ApolloConfigChangeListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 动态获取username
 */
@Service
public class MySQLConfigService {

    private static final Logger logger = LoggerFactory.getLogger(MySQLConfigService.class);

    private static final String prefix = "mysql";

    @ApolloConfig
    private Config config;

    @Autowired
    private MySQLPro mySQLPro;

    @ApolloConfigChangeListener
    private void onChange(ConfigChangeEvent changeEvent){
        Set<String> keyNames = config.getPropertyNames();
        for (String key : keyNames){
            logger.debug("the key is {}",key);
            if (containsIgnoreCase(key, prefix)){
                String username = config.getProperty("mysql.username", "admin-default");
                String password = config.getProperty("mysql.password", "psdDefault");
                Map map = new HashMap();
                map.put("listenerUsername", username);
                map.put("listerPassword", password);
                logger.debug("new params is {}",map);
                mySQLPro.setPassword(password);
                mySQLPro.setUsername(username);
            }
        }
    }

    /**
     * 比较str中是否包含了 searchStr
     * @param str 这里是配置中心所有key
     * @param searchStr 要匹配的key
     * @return
     */
    private static boolean containsIgnoreCase(String str, String searchStr) {
        if (str == null || searchStr == null) {
            return false;
        }
        int len = searchStr.length();
        int max = str.length() - len;
        for (int i = 0; i <= max; i++) {
            // 忽略大小写进行比较
            if (str.regionMatches(true, i, searchStr, 0, len)) {
                return true;
            }
        }
        return false;
    }

    public MySQLPro getMySQLPro() {
        return mySQLPro;
    }
}
