package com.tianju.contronller;

import com.tianju.config.MyConfig;
import com.tianju.config.MySQLConfigService;
import com.tianju.config.MySQLPro;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class ApolloController {

    private static Logger logger = LoggerFactory.getLogger(ApolloController.class);

    @Autowired
    private MyConfig myConfig;

    @Value("${mysql.password}")
    private String password;

    @Value("${myConfig}")
    private String str;

    @GetMapping("/get")
    public Map getConfig(){
        logger.debug(myConfig + "");
        Map map = new HashMap();
        map.put("username", myConfig.getUsername());
        map.put("password",password);
        map.put("myConfig",str);
        return map;
    }


    @Autowired
    private MySQLConfigService mySQLConfigService;

    @GetMapping("/get/apollo")
    public MySQLPro getConfigByApolloListener(){
        return mySQLConfigService.getMySQLPro();
    }
}
